

# Wordle_TiradoNerea



## Caracteristicas principales del juego 

Primero de todo explicaremos en que consiste la aplicación Wordle, es decir, sus caracteristicas principales.

Este juego consiste en adivinar la palabra en un máximo seis intentos y cinco letras mientras a su vez se va pintando el fondo de las letras dependiendo de su posición. Si su fondo aparece en gris quiere decir que su letra no existe, si aparece en amarillo son las letras que existen, pero no están en su posición indicada y por último tenemos el color verde que informa de la posición o palabra acertada.

Seguidamente, veremos que vamos restando aciertos y si no hemos acertado ninguna vez dentro de ese intervalo la palabra nos aparecerá la palabra que había que adivinar. También si el jugador decide seguir jugando la palabra irá cambiando por cada ronda iniciada.

Por último, al finalizar el juego veremos las estadísticas donde mostrara las jugadas realizadas, victoria, la mejor racha de todas y la distribución en cada momento. 

## Implementación del juego 

Este juego en vez de hacerlo como las otras veces hemos ido creando cada función como por ejemplo: principalmente que aparezcan todas las variables que hemos ido utilizando a lo largo de la creación del juego, una función que mediante un menú nos muestra las reglas, con el listado de palabras que guardara en su diccionario propio, generara una palabra aleatoria para cada partida iniciada y a su vez irá coloreando las letras, contar las letras y preguntarle al usuario si quiere seguir jugando o no.

Finalmente, acabará con un fun main donde llamaremos a todas las funciones.

## Extra 

En el proyecto anterior no mostraba la opción del menú con todas las letras y para que quedara más ordenador, explicado y mejor lo he añadido.
